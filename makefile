#lazy version nummering
VER = $(shell git rev-list HEAD --count)

#compiler, compiler flags, etc
CXX=cc -g
L=
D= -IInclude #-Llib
CFLAGS += -Wall -Wextra -Wpedantic \
	  -Wformat=2 -Wunused -Wno-unused-parameter -Wshadow \
	  -Wwrite-strings -Wstrict-prototypes -Wold-style-definition \
	  -Wredundant-decls -Wnested-externs -Werror

# GCC warnings that Clang doesn't provide:
ifeq ($(CCX),gcc)
	CFLAGS += -Wjump-misses-init -Wlogical-op
endif
CFLAGS += -std=c99
 
CF=$(D) $(L) -DVER=$(VER) $(CFLAGS)

#folders
BIN_FOLDER= bin
OBJ_FOLDER= obj
SRC_FOLDER= src

#target file
TARGET_FILE=main
TARGET=$(BIN_FOLDER)/$(TARGET_FILE)

# source & object files
SRCS  = $(subst $(SRC_FOLDER)/,,$(wildcard $(SRC_FOLDER)/*.c))
_OBJS = $(SRCS:.c=.o)
OBJS  = $(patsubst %,$(OBJ_FOLDER)/%,$(_OBJS))

# Have the compiler output dependency files with make targets for each
# of the object files. 
DEPENDENCIES = $(OBJS:.o=.d)

all: $(TARGET)

#dep:
#	@echo $(DEPENDENCIES)

$(TARGET): $(OBJS)
	$(CXX) -g $(OBJS) -o $(TARGET) $(CF)

$(OBJ_FOLDER)/%.o: $(SRC_FOLDER)/%.c
	$(CXX) $(CF) -c $< -o $@
	

clean:
	rm -f $(OBJ_FOLDER)/*.o
	rm -f $(BIN_FOLDER)/*

remake: clean all
