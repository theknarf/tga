# TGA files to black and white

By Frank Lyder Bredland

## Supported functions

Prioritet A:
-Det skal følge med en makefile som kompilerer programmet med parameterene
-std=c99 -Wall -Werror.
-Program skal ta en navnet på .tga-fil som parameter.
-Programmet må lagre en 8bit ukomprimert svarthvitt versjon av bildet. Det
må ikke skrive over det orginale bildet.
-Algoritmen som konverterer til svarthvitt må bruke alle farge kanalene.
-Algoritmen som konverterer til svarthvitt er valgritt, men må navngis og
dokumenteres.
-Programmet må takle 24bit ukomprimert RGB tga.
-Programmet må virke uavhengig av størrelse på bildet.

Prioritet B:
-Programmet skal evaluere om denne fila er av en type det håndterer.
-Håndterer programmet ikke fila skal de avslutte med en feilmelding om
hvorfor det ikke fungerte.
-Programmet må takle 32bit ukomprimert RGBA tga.
-Programmet må takle 16bit ukomprimert RGB tga.
-Programmet må skrive ut innholdet i Image Identification field.
-Programmet må håndtere et Image Identification field uavhengig av lengde.
-Dette feltet må kopieres over i det nye bildet.
-Programmet må akseptere kommandolinje-parametere som tillater å vende
bildet horisontalt og vertikalt.
-Vendig skal implementeres ved å endre headeren, ikke selve bildedataen.
-Makefile må støtte "clean" og "remake" med standard funksjonalitet.

## Not supported

Prioritet B:
-Programmet må takle en footer og skrive samme til det nye bildet.

Prioritet C:
-Programmet må takle color mapped TGA-filer.
-Programmet må takle komprimerte TGA-filer.

