#ifndef FILTER_BW_H
#define FILTER_BW_H

void convert_to_bw_32(void *input, void *output, void *input_max);
void convert_to_bw_24(void *input, void *output, void *input_max);
void convert_to_bw_16(void *input, void *output, void *input_max);

#endif
