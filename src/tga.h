#ifndef TGA_H
#define TGA_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef unsigned short WORD;
typedef unsigned char BYTE;

#pragma pack(push)  /* push current alignment to stack */
#pragma pack(1)     /* set alignment to 1 byte boundary */

typedef struct _TgaHeader
{
	BYTE IDLength;        /* 00h  Size of Image ID field */
	BYTE ColorMapType;    /* 01h  Color map type */
	BYTE ImageType;       /* 02h  Image type code */
	WORD CMapStart;       /* 03h  Color map origin */
	WORD CMapLength;      /* 05h  Color map length */
	BYTE CMapDepth;       /* 07h  Depth of color map entries */
	WORD XOffset;         /* 08h  X origin of image */
	WORD YOffset;         /* 0Ah  Y origin of image */
	WORD Width;           /* 0Ch  Width of image */
	WORD Height;          /* 0Eh  Height of image */
	BYTE PixelDepth;      /* 10h  Image pixel size */
	BYTE ImageDescriptor; /* 11h  Image descriptor byte */
} TGAHEAD;

typedef struct _Pixel_32
{
        uint8_t red, green, blue, alpha;
} PIXEL32;

typedef struct _Pixel_24
{
        uint8_t red, green, blue;
} PIXEL24;

typedef struct _Pixel_16
{
	BYTE data[2];
} PIXEL16;

#pragma pack(pop)   /* restore original alignment from stack */

TGAHEAD * gennerate_head_tga_8(BYTE IDLenght, WORD width, WORD height, BYTE ImageDescriptor);

#endif
