#include "tga.h"

#include <string.h>

TGAHEAD * gennerate_head_tga_8(BYTE IDLength, WORD width, WORD height, BYTE ImageDescriptor)
{
	TGAHEAD head = {
		.IDLength = IDLength,
		.ColorMapType = 0,
		.ImageType = 3,
		.CMapStart = 0,
		.CMapLength = 0,
		.CMapDepth = 0,
		.XOffset = 0,
		.YOffset = 0,
		.Width = width,
		.Height = height,
		.PixelDepth = 8,
		.ImageDescriptor = ImageDescriptor
	};

	TGAHEAD * tmp = calloc( 1, sizeof(TGAHEAD) );
	memcpy( tmp, &head, sizeof(TGAHEAD) );

	return tmp;
}
