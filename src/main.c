#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>

#include "tga.h"
#include "filter-bw.h"



int main(int argc, char *argv[])
{
	struct stat sb;
	// off_t len;
	char *p;
	int fd;
	int flip_vertical = 0, flip_horizontal = 0;

	if(argc < 3)
	{
		fprintf(stderr, "usage: <input> <output> [-vh]\n");
		return 1;
	}

	if(argc > 3)
	{
		if(strcmp(argv[3], "-v") == 0)
		{
			flip_vertical = 1;
		}
		
		if(strcmp(argv[3], "-h") == 0)
		{
			flip_horizontal	= 1;
		}

		if(strcmp(argv[3], "-hv") == 0 || strcmp(argv[3], "-vh") == 0)
		{
			flip_vertical = 1;
			flip_horizontal = 1;
		}
	}

	

	// Open the inputfile
	fd = open (argv[1], O_RDONLY);
	if (fd == -1) {
		perror ("open");
		return 1;
	}

	if (fstat (fd, &sb) == -1) {
		perror ("fstat");
		return 1;
	}

	if (!S_ISREG (sb.st_mode)) {
		fprintf (stderr, "%s is not a file\n", argv[1]);
		return 1;
	}

	p = mmap (0, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (p == MAP_FAILED) {
		perror ("mmap");
		return 1; 
	}

	if (close (fd) == -1) {
		perror ("close");
		return 1; 
	}

	// Do shit
	TGAHEAD * tmp = (TGAHEAD *) p;
	
	void * output_data = calloc( tmp->Width * tmp->Height , sizeof(uint8_t) );

	BYTE imgdes = tmp->ImageDescriptor;

	if(flip_horizontal == 1)
	{
		imgdes ^= 0x20;
	}

	if(flip_vertical == 1)
	{
		imgdes ^= 0x20;
	}


	TGAHEAD * newhead = gennerate_head_tga_8(tmp->IDLength, tmp->Width, tmp->Height, imgdes);

	if(newhead->ImageType != 2 && newhead->ImageType != 3)
	{
		perror("Image type not supportet, must not be encoded and not colormapped\n");
		return 1;
	}

	void * imgdata = (void *)(p + sizeof(TGAHEAD) + tmp->IDLength);

	if(tmp->PixelDepth == 32)
	{
		convert_to_bw_32(imgdata, output_data, (void *)((char *)imgdata + tmp->Width * tmp->Height * tmp->PixelDepth / 8));
	}
	else if(tmp->PixelDepth == 24)
	{
		convert_to_bw_24(imgdata, output_data, (void *)((char *)imgdata + tmp->Width * tmp->Height * tmp->PixelDepth / 8));
	}
	else if(tmp->PixelDepth == 16)
	{
		convert_to_bw_16(imgdata, output_data, (void *)((char *)imgdata + tmp->Width * tmp->Height * tmp->PixelDepth / 8));
	}
	else
	{
		perror("pixeldepth not supportet");
		return 1;
	}

	// Output file
	FILE * file_out = fopen(argv[2], "w");
	
	fwrite(newhead, sizeof(TGAHEAD), 1, file_out);
	fwrite(imgdata, sizeof(BYTE), tmp->IDLength, file_out);
	fwrite(output_data, sizeof(uint8_t), tmp->Width * tmp->Height, file_out);
	

	fclose(file_out);

	// Unmap
	if (munmap (p, sb.st_size) == -1) {
		perror ("munmap");
		return 1; 
	}

	free(newhead);
	free(output_data);

	return 0;
}
