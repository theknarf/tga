#include "filter-bw.h"
#include "tga.h"
#include <string.h>
#include <stdio.h>

uint8_t luma(int r, int g, int b)
{
        float l = 0.299 * (float)r + 0.587 * (float)g + 0.114 * (float)b;

        if ( l > 255)
                l = 255;

        return (uint8_t)l;
}

void convert_to_bw_32(void * input, void * output, void * input_max)
{
        PIXEL32 * pixeldata;
	uint8_t * p_output;

	pixeldata = (PIXEL32 *) input;
	p_output = (uint8_t *) output;

        while ( (void *)pixeldata < input_max )
        {
               uint8_t l = luma(pixeldata->red, pixeldata->green, pixeldata->blue);

                memcpy( p_output, &l, sizeof(uint8_t));
		
		pixeldata++;
		p_output++;
        }

}

void convert_to_bw_24(void * input, void * output, void * input_max)
{
        PIXEL24 * pixeldata;
	uint8_t * p_output;

	pixeldata = (PIXEL24 *) input;
	p_output = (uint8_t *) output;

        while ( (void *)pixeldata < input_max )
        {
               uint8_t l = luma(pixeldata->red, pixeldata->green, pixeldata->blue);

                memcpy( p_output, &l, sizeof(uint8_t));
		
		pixeldata++;
		p_output++;
        }

}


void convert_to_bw_16(void * input, void * output, void * input_max)
{
        PIXEL16 * pixeldata;
	uint8_t * p_output;
	int red, green, blue;

	pixeldata = (PIXEL16 *) input;
	p_output = (uint8_t *) output;

        while ( (void *)pixeldata < input_max )
	{
		red      = (pixeldata->data[0] & 0xfc00) >> 11;
		green    = (pixeldata->data[0] & 0x07e0) >> 6;
		blue     = (pixeldata->data[0] & 0x003f) >> 1;

		uint8_t l = luma(red, green, blue) * 10;
		printf("%i\n", l);

		memcpy( p_output, &l, sizeof(uint8_t));

		pixeldata++;
		p_output++;
	}

}
